import os, sys
from argparse import ArgumentParser
from datetime import datetime as dt
from pprint import pprint as pp
import shutil, glob
from pyfiglet import figlet_format, Figlet
import logging

'''
python run_approxilyzer.py -a min_max -r x86 -d x86root-parsec.img -k vmlinux-4.9.113 -n 8
'''

def run_approxilyzer(apps, config):

    for app in apps:
        figlet_print('Step 1')
        MAIN_START, MAIN_END = step_1(app, config['WORKLOAD_APPS'])
        figlet_print('Step 2')
        step_2(app, config['WORKLOAD_APPS'], config['WORKLOAD_CPT'])
        step_3_and_4_and_5(app, config['GEM5_FAST'], config['GEM5'], config['DISK'], config['KERNEL'], \
                config['DISK_NAME'], config['KERNEL_NAME'], config['SCRIPTS'], config['WORKLOAD_CPT'])
        step_6_and_7(app, config['WORKLOAD_APPS'], config['ARCH'].lower(), config['GEM5'], \
                MAIN_START, MAIN_END, config['DISK_NAME'])
        figlet_print('Step 8')
        step_8(app, config['GEM5'], config['WORKLOAD_APPS'], config['ARCH'].lower(), config['NUM_PROCS'], \
                config['DISK_NAME'])
        figlet_print('Step 9')
        step_9(app, config['GEM5'], config['ARCH'].lower())

    return

def step_1(app, wadir):
    # NOTE: Checked 
    if not os.path.isfile(os.path.join(wadir, app, app)) or \
            not os.access(os.path.join(wadir, app, app), os.X_OK):
        cmd = 'gcc -o ' + os.path.join(wadir, app, app) + ' -gdwarf-2 ' + os.path.join(wadir, app, app + '.c')
        print_info(cmd)
        status = os.system(cmd)
        if status != 0:
            print_fail('Compilation of app: ' + app + ' failed')
            exit(-1)
    
    if not os.path.isfile(os.path.join(wadir, app, app + '.dis')):
        cmd = 'objdump -D -S ' + os.path.join(wadir, app, app) + ' > ' + os.path.join(wadir, app, app + '.dis')
        print_info(cmd)
        status = os.system(cmd)
        if status != 0:
            print_fail('Object dump for app: ' + app + ' failed')
            exit(-1)

    f = open(os.path.join(wadir, app, app + '.dis'), 'r')
    l = f.readlines()
    f.close()

    ROI_idx = [idx for idx in range(len(l)) if 'asm' in l[idx]]
    
    MAIN_START = l[ROI_idx[0] + 1].split()[0][:-1]
    MAIN_END = l[ROI_idx[1] + 1].split()[0][:-1]
    
    return MAIN_START, MAIN_END

def step_2(app, wadir, wcdir):

    if not os.path.isdir(os.path.join(wadir, app)):
        print_fail('Workload app directory for app: ' + app + ' does not exist')
        exit(-1)

    if not os.path.isdir(os.path.join(wcdir, app)):
        print_fail('Workload checkpoint directory for app: ' + app + ' does not exist')
        exit(-1)

    print_info('Both app and checkpoint directory for app: ' + app + ' exist')

    return

def step_3_and_4_and_5(app, gem5_fast, gem5, disk, kernel, disk_name, kernel_name, scripts, wcdir):

    old_dir = os.getcwd()

    os.chdir(gem5)

    figlet_print('Step 3')
    cmd = gem5_fast + ' ' + os.path.join(gem5, 'configs/example', 'fs.py') + \
            ' --disk-image=' + os.path.join(disk, disk_name) + \
            ' --kernel=' + os.path.join(kernel, kernel_name) + \
            ' --script ' + os.path.join(scripts, app + '.rcS')
    print_info(cmd)
    status = os.system(cmd)

    if status != 0:
        print_fail('Checkpoint creation failed for app: ' + app)
        exit(-1)
    
    # NOTE: Needs to be fixed ASAP
    figlet_print('Step 4')
    d = os.path.join(gem5, 'm5out')
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    cpt_dir_name = dirs[0][dirs[0].rfind('/') + 1:]
    if not os.path.isdir(os.path.join(wcdir, app, cpt_dir_name)):
        os.mkdir(os.path.join(wcdir, app, cpt_dir_name))
    else:
        shutil.rmtree(os.path.join(wcdir, app, cpt_dir_name))
        os.mkdir(os.path.join(wcdir, app, cpt_dir_name))

    move_file_src_dest(dirs[0], os.path.join(wcdir, app, cpt_dir_name))
    
    src_file = os.path.join(gem5, 'm5out', 'output.txt')
    dest_file = os.path.join(wcdir, app, app + '.output')

    figlet_print('Step 5')
    if os.path.islink(dest_file):
        os.unlink(dest_file)

    os.symlink(src_file, dest_file)

    os.chdir(old_dir)

    return

def step_6_and_7(app, wadir, arch, gem5, MAIN_START, MAIN_END, disk_name):

    old_dir = os.getcwd()

    figlet_print('Step 6')
    os.chdir(os.path.join(gem5, 'scripts/relyzer'))

    cmd = 'python inst_database.py ' + os.path.join(wadir, app, app + '.dis') + \
            ' ' + os.path.join(wadir, app, app + '_parsed.txt')
    print_info(cmd)
    status = os.system(cmd)
    if status != 0:
        print_fail('Creation of instruction data based failed for app: ' + app)
        exit(-1)

    cmd = 'sh gen_exec_trace.sh' + ' ' + arch + ' ' + app + ' ' + disk_name
    print_info(cmd)
    status = os.system(cmd)
    if status != 0:
        print_fail('Execution trace generation failed for app: ' + app)
        exit(-1)

    cmd = 'sh gen_mem_trace.sh' + ' ' + arch + ' ' + app + ' ' + disk_name
    print_info(cmd)
    status = os.system(cmd)
    if status != 0:
        print_fail('Memory trace generation failed for app: ' + app)
        exit(-1)
    cmd = 'python gen_simplified_trace.py' + ' ' + app + ' ' + \
            '0x' + MAIN_START + ' ' + '0x' + MAIN_END + ' ' + arch 
    print_info(cmd)
    status = os.system(cmd)
    if status != 0:
        print_fail('Simplified trace generation failed for app: ' + app)
        exit(-1)

    figlet_print('Step 7')
    cmd = 'sh relyzer.sh' + ' ' + app + ' ' + arch + ' 100' 
    print_info(cmd)
    status = os.system(cmd)
    if status != 0:
        print_fail('Relyzer analysis failed for app: ' + app)
        exit(-1)

    os.chdir(old_dir)

    return

def step_8(app, gem5, wadir, arch, num_procs, disk_name):

    old_dir = os.getcwd()

    os.chdir(os.path.join(gem5, 'scripts/injections'))

    cmd = 'sh run_jobs_parallel.sh' + ' ' + os.path.join(wadir, app, app + '_inj_100_list.txt') + ' ' + \
            arch + ' ' + app + ' 1 ' + app + '.output ' + num_procs + ' ' + disk_name
    print_info(cmd)
    status = os.system(cmd)
    if status != 0:
        print_fail('Faul injection failed for the app: ' + app)
        exit(-1)

    os.chdir(old_dir)

    return

def step_9(app, gem5, arch):

    old_dir = os.getcwd()

    os.chdir(os.path.join(gem5, 'outputs', arch))

    cmd = 'cat ' + app + '-* > ' + app + '.outcomes_raw'
    print_info(cmd)
    status = os.system(cmd)
    if status != 0:
        print_fail('Concatenating raw outputs failed for the app: ' + app)
        exit(-1)

    os.chdir(os.path.join(gem5, 'scripts/relyzer'))
    
    cmd = 'python postprocess.py' + ' ' + app + ' ' + arch
    status = os.system(cmd)
    print_info(cmd)
    if status != 0:
        print_fail('Posprocessing raw data failed for app: ' + app)
        exit(-1)

    os.chdir(old_dir)

    return


def move_file_src_dest(srcDir, dstDir):
    if os.path.isdir(srcDir) and os.path.isdir(dstDir) :
        for filePath in glob.glob(srcDir + '/*'):
            shutil.move(filePath, dstDir);
    else:
        print_fail("srcDir & dstDir should be Directories") 
        exit(-1)

    return

def main(apps, arch, disk, kernel, num_procs):

    # Setting up bash script and workload folder hierarchy for the Approxilyzer run
    status = os.system('sh install.sh')
    if status == 0:
        os.environ['APPROXGEM5'] = os.getcwd()
    else:
        exit(-1)
    
    # To store all pertinent information
    config = {}
    # Setting up APPROXGEM5 environment variable for everything else going forward
    APPROXGEM5 = os.getenv('APPROXGEM5')
    
    config['GEM5'] = os.path.join(APPROXGEM5, 'gem5')
    config['KERNEL'] = os.path.join(APPROXGEM5, 'dist/m5/system/binaries')
    config['DISK'] = os.path.join(APPROXGEM5, 'dist/m5/system/disks')
    config['SCRIPTS'] = os.path.join(APPROXGEM5, 'checkpoint_scripts')
    config['ARCH'] = arch.upper()
    config['DISK_NAME'] = disk
    config['KERNEL_NAME'] = kernel
    config['NUM_PROCS'] = num_procs
    config['WORKLOAD_APPS']  = os.path.join(APPROXGEM5, 'workloads', arch, 'apps')
    config['WORKLOAD_CPT']  = os.path.join(APPROXGEM5, 'workloads', arch, 'checkpoint')
    
    GEM5_FAST = os.path.join(config['GEM5'], 'build', config['ARCH'], 'gem5.fast')
    GEM5_OPT = os.path.join(config['GEM5'], 'build', config['ARCH'], 'gem5.opt')

    if not os.path.isfile(GEM5_FAST) or not os.access(GEM5_FAST, os.X_OK):
        print_fail('gem5.fast executable is not available in the path ' + os.path.join(config['GEM5'], 'build'))
        exit(-1)
    else:
        config['GEM5_FAST'] = GEM5_FAST

    if not os.path.isfile(GEM5_OPT) or not os.access(GEM5_OPT, os.X_OK):
        print_fail('gem5.opt executable is not available in the path ' + os.path.join(config['GEM5'], 'build'))
        exit(-1)
    else:
        config['GEM5_OPT'] = GEM5_OPT

    run_approxilyzer(apps, config)
    
    return

def figlet_print(text):
    (width, height) = getTerminalSize()
    f = Figlet(font='slant', direction=1, justify='center', width=width)
    print(f.renderText(text))
    return


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKBLACK  = "\033[90m"
    OKBCKGRD = "\033[47m"
    OKGREEN = '\033[92m'
    OKTEAL = '\033[96m'
    TRY = '\033[97m'
    WARNING = '\033[93m'
    BCKGRD_WARNING = "\033[93m"
    FAIL = '\033[91m'
    BCKGRD_FAIL = "\033[93m"
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_warning(print_string):
    logger = logging.getLogger('approxilyzer')
    logger.warning(print_string)
    print(bcolors.BOLD  + bcolors.OKBLACK + bcolors.BCKGRD_WARNING + "[WARN]--> " + \
            print_string + bcolors.ENDC + " ")

def print_info(print_string):
    logger = logging.getLogger('approxilyzer')
    logger.info(print_string)
    print(bcolors.BOLD + bcolors.OKTEAL + bcolors.OKGREEN + "[INFO]--> " + print_string + bcolors.ENDC + " ")

def print_fail(print_string):
    logger = logging.getLogger('approxilyzer')
    logger.critical(print_string)
    print(bcolors.BOLD  + bcolors.OKBLACK + bcolors.FAIL +  "[FAIL]--> " + print_string + bcolors.ENDC + " ")


def getTerminalSize():
    import os
    env = os.environ
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ, '1234'))
        except:
            return
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass

    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))
    return int(cr[1]), int(cr[0])

def approx_logger(name, logname):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - \
            - %(message)s')
    handler = logging.FileHandler(logname, mode='w')
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    return logger

def app(args):
    
    if not args:
        return []
    else:
        return args.split(',')
    

if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-a', "--apps", help='Target application names seperated by comma', \
            dest='targetapp', required=True)
    parser.add_argument('-r', "--architecture", help='Target application architecture', \
            dest='arch', default='x86')
    parser.add_argument('-d', "--disk_image", help='Disk image name', \
            dest='disk', required=True)
    parser.add_argument('-k', "--kernel_binary", help='Kernel binary name', \
            dest='kernel', required=True)
    parser.add_argument('-n', "--num_procs", \
            help='Number of processors to be used for parallel fault injection', \
            dest='num_procs', required=True)


    args = parser.parse_args()

    apps = app(args.targetapp)

    if not apps:
        print_fail('No app specified. Exiting...')
    
    logname = 'approxilyzer.log'
    alogger = approx_logger('approxilyzer', logname)

    main(apps, args.arch, args.disk, args.kernel, args.num_procs)
